:set expandtab
:set shiftwidth=2
:set tabstop=2

execute pathogen#infect()
syntax on
filetype plugin indent on
